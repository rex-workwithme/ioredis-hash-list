const express = require('express');
const { key, getRedis } = require('./config');

const { redis } = getRedis;

const PORT = process.env.PORT || 5000;

const app = express();
app.use(express.json());
app.get('/getdata', async (req, res) => {
  try {
    const allHash = await redis.lrange(key, 0, -1);
    const arr = await Promise.all(allHash.map(async (x) => {
      const value = await redis.hgetall(`id:${x}`);
      return value;
    }));
    console.log(arr);
    res.status(200).json({
      success: true,
      data: arr,
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({
      success: false,
      message: 'There is some error!',
    });
  }
});

app.get('/getdata/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const result = await redis.hgetall(`id:${id}`);
    if (JSON.stringify(result) !== '{}') {
      console.log(result);
      res.status(200).json({
        success: true,
        data: result,
      });
    } else {
      res.status(404).json({
        success: false,
        message: 'Data not found!',
      });
    }
  } catch (error) {
    console.error(error);
    res.status(400).json({
      success: false,
      message: 'There is some error!',
    });
  }
});

app.post('/postdata', async (req, res) => {
  try {
    const { id = new Date().valueOf(), name, age } = req.body;
    console.log(id);
    const hashKey = `id:${id}`; // Used for hashname
    const dataSize = await redis.lpush(key, id);
    await redis.hset(hashKey, 'id', id, 'name', name, 'age', age);
    if (dataSize >= 10) {
      const selectedItem = await redis.lindex(key, -1);
      console.log(selectedItem); // To check which item will be deleted
      await redis.ltrim(key, 0, 9);
      await redis.hdel(`id:${selectedItem}`, 'name', 'age');
    }

    res.status(200).json({
      success: true,
      message: 'Data inserted successfully!',
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({
      success: false,
      message: 'There is some error!',
    });
  }
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
